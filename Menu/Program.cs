﻿namespace Menu
{
    internal class Program
    {
        static void Main(string[] args)
        {
            Menu menu = new();

            menu.AddMenuItem("One", ClearOptions.ClearAndDisplay);
            menu.AddMenuItem("Two", ClearOptions.ClearAndDisplay);
            menu.AddMenuItem("Three", ClearOptions.ClearAndDisplay);
            menu.AddMenuItem("Four", ClearOptions.ClearAndDisplay);

            Console.CursorVisible = false;

            while (true)
            {
                ConsoleKeyInfo keyInfo = Console.ReadKey();

                menu.HandleInput(ref keyInfo, ClearOptions.ClearAndDisplay);

            }

        }


    }
}