﻿namespace Menu
{
    class MenuOption
    {
        public MenuOption(string _menu, bool _selected)
        {
            menu = _menu;
            selected = _selected;
        }

        public string menu;
        public bool selected;
    }

    public enum ClearOptions { DoNotClearOrDisplay, DisplayButNotClear, ClearAndDisplay }

    public class Menu
    {

        List<MenuOption> m_Options = new();
        int m_SelectedOption = 0;

        public void AddMenuItem(string menuText, ClearOptions clearOption)
        {
            m_Options.Add(new(menuText, (m_Options.Count < 1) ? true : false));
            UpdateDisplay(clearOption);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="keyInfo"></param>
        /// <param name="clearOption"></param>
        /// <returns>If a valid key was pressed</returns>
        public bool HandleInput(ref ConsoleKeyInfo keyInfo, ClearOptions clearOption)
        {
            switch (keyInfo.Key)
            {
                case ConsoleKey.DownArrow:
                    if (m_SelectedOption < m_Options.Count - 1)
                        m_SelectedOption++;
                    else if (m_SelectedOption == m_Options.Count - 1)
                        m_SelectedOption = 0;
                    break;
                case ConsoleKey.UpArrow:
                    if (m_SelectedOption == 0)
                        m_SelectedOption = m_Options.Count - 1;
                    else
                        m_SelectedOption--;
                    break;
                case ConsoleKey.Enter:
                    return true;
                default:
                    return false;
            }

            foreach (MenuOption option in m_Options)
            {
                option.selected = false;
            }

            m_Options[m_SelectedOption].selected = true;

            UpdateDisplay(clearOption);

            return true;
        }

        public void UpdateDisplay(ClearOptions clearOptions)
        {
            if (clearOptions == ClearOptions.DoNotClearOrDisplay)
                return;

            if (clearOptions == ClearOptions.ClearAndDisplay)
                Console.Clear();

            foreach (MenuOption option in m_Options)
            {
                if (option.selected)
                {
                    Console.BackgroundColor = ConsoleColor.DarkGray;
                    Console.ForegroundColor = ConsoleColor.White;
                }
                else
                {
                    Console.ForegroundColor = ConsoleColor.White;
                }

                Console.WriteLine(option.menu);
                Console.ResetColor();
            }
        }

    }
}
